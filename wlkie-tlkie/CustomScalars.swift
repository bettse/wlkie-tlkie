//
//  AudioType.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 1/31/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import Foundation
import Apollo

public typealias Audio = [String : Any?]
public typealias Selection = [String: [String: Bool]]


extension Dictionary: JSONDecodable {
  /// Custom `init` extension so Apollo can decode custom scalar type
  public init(jsonValue value: JSONValue) throws {
    guard let dictionary = value as? Dictionary else {
      throw JSONDecodingError.couldNotConvert(value: value, to: Dictionary.self)
    }
    self = dictionary
  }
}

// The graphql custom scalar of this schema is a ISO8601 formatted string
extension Date: JSONDecodable {
  /// Custom `init` extension so Apollo can decode custom scalar type
  public init(jsonValue value: JSONValue) throws {
    let dateFormatter = ISO8601DateFormatter()
    dateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
    guard let string = value as? String else {
      throw JSONDecodingError.couldNotConvert(value: value, to: String.self)
    }    
    guard let date = dateFormatter.date(from: string) else {
        throw JSONDecodingError.couldNotConvert(value: string, to: Date.self)
    }
    self = date
  }
}

