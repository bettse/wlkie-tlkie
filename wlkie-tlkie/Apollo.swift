//
//  Apollo.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 1/30/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import Foundation
import Apollo
import ApolloWebSocket
import SwiftUI // UIApplication

// MARK: - Singleton Wrapper

class Apollo {
  static let shared = Apollo()
  
  /// An HTTP transport to use for queries and mutations
  private lazy var httpTransport: HTTPNetworkTransport = {
    let defaults = UserDefaults.standard
    let graphql = defaults.object(forKey: "graphql") as? String ?? "http://ataraxia.ericbetts.org:3000/graphql"
    let url = URL(string: graphql)!
    //print("URL = \(url)")
    return HTTPNetworkTransport(url: url)
  }()

  /// Create a client using the `SplitNetworkTransport`.
  private(set) lazy var client = ApolloClient(networkTransport: self.httpTransport)
}
