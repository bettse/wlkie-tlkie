//
//  ReviewViewController.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 1/30/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import Foundation
import SwiftUI
import Apollo
import AVFoundation

class CallsStore : NSObject, ObservableObject, AVAudioPlayerDelegate {
    static let shared = CallsStore()
    let apollo = Apollo.shared.client
    let defaults = UserDefaults.standard

    @Published private(set) var calls: [RdioScannerCall] = []
    @Published private(set) var systems : [RdioScannerSystem] = []

    var audioplayer : AVAudioPlayer?
    @Published var active : Bool = true {
        didSet {
            if (!oldValue) { // was paused
                self.playNext()
            }
        }
    }

    func registerPush(deviceToken: String) {
        var selection : String = "{}"
        do {
            if let selectionDict = defaults.object(forKey: "selection") as? Selection {
                let selectionData = try JSONSerialization.data(withJSONObject: selectionDict, options: [])
                selection = String(data: selectionData, encoding: .utf8) ?? ""
            }
        } catch { print("registerPush error: \(error)") }
        print("DEBUG: selection = \(selection)")
        apollo.perform(mutation: ApnsDeviceMutation(selection: selection, deviceToken: deviceToken)) { result in
          switch result {
          case .success(let graphQLResult):
            if let result = graphQLResult.data?.apnsDevice {
                if (result) {
                    print("Registered for push")
                } else {
                    print("Problem registering for push")
                }
            } else if let errors = graphQLResult.errors {
              // GraphQL errors
              print(errors)
            }
          case .failure(let error):
            // Network or response format errors
            print(error)
          }
        }
    }

    func loadSystems() {
        apollo.fetch(query: RdioScannerSystemsQuery()) { result in
          switch result {
          case .success(let graphQLResult):
            if let systems = graphQLResult.data?.rdioScannerSystems as? [RdioScannerSystemsQuery.Data.RdioScannerSystem] {
                self.systems = systems.map({ (system) -> RdioScannerSystem in
                    return RdioScannerSystem(system: system)
                })
                print("Systems downloaded")
            } else if let errors = graphQLResult.errors {
              // GraphQL errors
              print(errors)
            }
          case .failure(let error):
            // Network or response format errors
            print(error)
          }
        }
    }

    func playSilence() {
        //Play silence to setup audioSession and stuff (hand wavy)
        let silence = Bundle.main.path(forResource: "silence.m4a", ofType: nil)
        let url = URL(fileURLWithPath: silence!)
        do {
            audioplayer = try AVAudioPlayer(contentsOf: url)
            audioplayer?.delegate = self
            audioplayer?.play()
        } catch {
            print("Failed to play silence")
        }
    }

    func activateDuckingSession() throws {
        let audioSession = AVAudioSession.sharedInstance()
        //print("other: \(audioSession.isOtherAudioPlaying) | secondary: \(audioSession.secondaryAudioShouldBeSilencedHint)")
        //NB: .defaultToSpeaker only valid for .playAndRecord
        //, .allowBluetoothA2DP, .allowBluetooth, .allowAirPlay
        try audioSession.setCategory(.playback, mode: .default, options: [.duckOthers])
        try audioSession.setActive(true)
    }

    func getCall(id: String, completion: (() -> Void)? = nil) {
        apollo.fetch(query: RdioScannerCallQuery(id: id)) { result in
          switch result {
          case .success(let graphQLResult):
            if let call = graphQLResult.data?.rdioScannerCall {
                let system = self.systems.first(where: {$0.id == call.system})!
                self.calls.append(RdioScannerCall(call: call, system: system))
                print("got call \(String(describing: call.id))")
                self.playNext()
                if let completion = completion {
                    completion()
                }
            } else if let errors = graphQLResult.errors {
                 // GraphQL errors
                 print(errors)
            }
          case .failure(let error):
            // Network or response format errors
            print(error)
          }
        }
    }

    func playNext() {
        print("playNext")
        if !self.active || self.calls.isEmpty || self.audioplayer!.isPlaying {
            return
        }
        do {
            try self.activateDuckingSession()
        } catch {
            print("Couldn't activate audio session, skipping")
            self.calls[-1].errorPlaying = error.localizedDescription
            return
        }

        guard let call = self.calls.first else {
            return
        }
        self.calls[0].playing = true

        do {
            audioplayer = try AVAudioPlayer(data: call.audio, fileTypeHint: "m4a")
        } catch {
            print("failed to create player")
        }

        if let audioplayer = audioplayer {
            audioplayer.delegate = self
            audioplayer.play()
            print("playing \(call.id)")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("audioPlayerDidFinishPlaying \(flag)")
        if (!player.isPlaying) {
            //2020-02-05 14:31:07.851495-0800 wlkie-tlkie[6702:2747506] [avas] AVAudioSession.mm:997:-[AVAudioSession setActive:withOptions:error:]: Deactivating an audio session that has running I/O. All I/O should be stopped or paused prior to deactivating the audio session.
            do { try AVAudioSession.sharedInstance().setActive(false) } catch { }
        }
        if (!self.calls.isEmpty) {
            self.calls.removeFirst()
        }
        self.playNext()
    }
}
