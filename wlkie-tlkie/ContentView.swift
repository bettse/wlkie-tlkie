//
//  ContentView.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 1/30/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import SwiftUI

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(CallsStore.shared)
    }
}

struct ContentView: View {
    @EnvironmentObject var callStore: CallsStore

    var body: some View {
        VStack(alignment: .center) {
            HStack() {
                Button(action: {
                    self.callStore.active = false
                }) {
                    Image(systemName: self.callStore.active ? "pause.rectangle" : "pause.rectangle.fill")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                }
                Button(action: {
                    self.callStore.active = true
                }) {
                    Image(systemName: self.callStore.active ? "play.rectangle.fill" : "play.rectangle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                }
            }

            NavigationView {
                List(callStore.calls) { call in
                    RdioScannerCallCell(call: call)
                }
                .navigationBarTitle(Text("Call Queue"), displayMode: .inline)
                .navigationBarItems(
                    leading: NavigationLink(destination: SettingsView(systems: callStore.systems)) {
                        Image(systemName: "gear")
                            .disabled(callStore.systems.count == 0)
                    }.buttonStyle(PlainButtonStyle()),
                    trailing: Button(action: {
                        // TODO: Check if audioPlayer exists and is not playing first
                        self.callStore.playNext()
                    }) {
                        Image(systemName: "forward.fill")
                            .disabled(callStore.calls.count == 0)
                    }
                )
            }
        }
        .padding()
        .onAppear(perform: start)
    }
    
    private func start() {
        callStore.playSilence()
    }
}
