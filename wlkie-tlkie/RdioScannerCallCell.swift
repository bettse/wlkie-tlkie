//
//  RdioScannerCallCell.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 2/7/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import SwiftUI

struct RdioScannerCallCell_Previews: PreviewProvider {
    
    static var previews: some View {
        RdioScannerCallCell(call: fakeCall())
    }

    static func fakeCall() -> RdioScannerCall {
        var audio : Audio = Audio()

        let silence = Bundle.main.path(forResource: "silence.m4a", ofType: nil)
        let url = URL(fileURLWithPath: silence!)
        do {
            let rawAudio = try Data(contentsOf: url)
            audio = ["data": [UInt8](rawAudio)]
        } catch {
            print("callsStore(debug: true): \(error)")
        }

        let stopTime = Date()
        let startTime = stopTime.addingTimeInterval(-10.5)

        let talkgroup = RdioScannerSystemsQuery.Data.RdioScannerSystem.Talkgroup(alphaTag: "fake TG", dec: 2, description: "My fake talkgroup", group: "group", mode: "A", tag: "tag")
        let system = RdioScannerSystem(system: RdioScannerSystemsQuery.Data.RdioScannerSystem(id: "1", name: "fake", system: 11, talkgroups: [talkgroup]))

        let rawCall = RdioScannerCallQuery.Data.RdioScannerCall(id: "10244", audio: audio, emergency: false, freq: 463638000, startTime: startTime, stopTime: stopTime, system: system.id, talkgroup: talkgroup.dec)
        var call = RdioScannerCall(call: rawCall, system: system)
        call.playing = true
        return call
    }
}

struct RdioScannerCallCell: View {
    let call: RdioScannerCall
    var duration : TimeInterval {
        return self.call.duration
    }
    var body: some View {
        HStack(alignment: .center) {
            Image(systemName: call.playing ? "waveform.circle.fill" : "waveform.circle")
                .font(.largeTitle)

            VStack(alignment: .leading) {
                Text(formatDate(date: call.startTime))
                    .font(.title)
                Text(call.talkgroup.alphaTag)
                Text(call.playing ? "\(duration.description)s" : call.errorPlaying)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                    .multilineTextAlignment(.leading)
            }
        }
    }

    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm:ss a"
        return dateFormatter.string(from: date)
    }
}
