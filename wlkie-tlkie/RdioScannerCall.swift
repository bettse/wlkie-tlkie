//
//  RdioScannerCallCell.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 1/30/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import SwiftUI
import AVFoundation

struct RdioScannerCall: Identifiable, Hashable {
    let id: Int
    let freq: Int
    let startTime: Date
    let endTime: Date
    let system: String
    let talkgroup: RdioScannerTalkgroup
    let audio: Data
    var errorPlaying : String = ""
    var playing : Bool = false
    var duration : TimeInterval {
        do {
            let audioplayer = try AVAudioPlayer(data: self.audio, fileTypeHint: "m4a")
            return audioplayer.duration
        } catch {
            return 0.0
        }
    }

    init(call: RdioScannerCallQuery.Data.RdioScannerCall, system: RdioScannerSystem) {
        self.id = Int(call.id!)!

        self.freq = call.freq!
        self.startTime = call.startTime!
        self.endTime = call.stopTime!

        self.system = system.name
        self.talkgroup = system.talkgroups.first(where: {$0.id == call.talkgroup!})!

        let audio = call.audio!
        let data = audio["data"] as! [UInt8]
        self.audio = Data(data)
    }
}
