//
//  SettingsView.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 2/7/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import SwiftUI

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(systems: [])
    }
}

struct SettingsView: View {
    let systems : [RdioScannerSystem]
    @State private var graphql: String = UserDefaults.standard.object(forKey: "graphql") as? String ?? ""

    var body: some View {
        VStack() {
            VStack(alignment: .leading) {
                List {
                    ForEach(systems) { system in
                        Section(header: Text(system.name)) {
                            ForEach(system.talkgroups, id: \.self) { (talkgroup) in
                                TalkgroupOption().environmentObject(talkgroup)
                            }
                        }
                    }
                }.listStyle(GroupedListStyle())
            }
            VStack(alignment: .leading) {
                Text("rdio-scanner graphql endpoint").font(.headline)
                TextField("http://myhost.tld/graphql:port", text: $graphql)
                    .border(Color.gray)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
            }.padding()

            Button(action: onSave) {
                Text("Save")
            }
        }
    }

    func onSave() {
        let defaults = UserDefaults.standard
        defaults.set(self.graphql, forKey: "graphql")

        //Build JSON for selection
        var selection : Selection = [:]
        self.systems.forEach { (system) in
            var activeTalkgroups : [String:Bool] = [:]
            system.talkgroups.forEach { (talkgroup) in
                // Add if true or if updated from old value
                if (talkgroup.enabled || talkgroup.dirty) {
                    activeTalkgroups["\(talkgroup.id)"] = talkgroup.enabled
                }
            }
            if (activeTalkgroups.count > 0) {
                selection["\(system.id)"] = activeTalkgroups
            }
        }

        print("New selection: \(selection)")
        defaults.set(selection, forKey: "selection")
        //TODO: Figure out how to call registerPush(deviceToken) again
    }
}

struct TalkgroupOption : View {
    @EnvironmentObject var talkgroup: RdioScannerTalkgroup

    var body : some View {
        Toggle(talkgroup.alphaTag, isOn: $talkgroup.enabled)
    }
}
