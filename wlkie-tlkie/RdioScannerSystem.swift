//
//  RdioScannerSystem.swift
//  wlkie-tlkie
//
//  Created by Eric Betts on 2/5/20.
//  Copyright © 2020 Eric Betts. All rights reserved.
//

import Foundation
import SwiftUI

struct RdioScannerSystem: Identifiable {
    let defaults = UserDefaults.standard
    let id : Int
    let name : String
    let talkgroups: [RdioScannerTalkgroup]
    let dict = UserDefaults.standard.object(forKey: "selection") as? Selection ?? Selection()

    init(system: RdioScannerSystemsQuery.Data.RdioScannerSystem) {
        self.id = system.system!
        self.name = system.name!
        let enabled : [String:Bool] = dict["\(id)"] ?? [:]

        let talkgroups = system.talkgroups! as! [RdioScannerSystemsQuery.Data.RdioScannerSystem.Talkgroup]
        self.talkgroups = talkgroups.map({ (talkgroup) -> RdioScannerTalkgroup in
            let rst = RdioScannerTalkgroup(talkgroup: talkgroup)
            rst.enabled = enabled["\(rst.id)"] ?? false
            return rst
        })
    }
}

// Question: nest this in System?
final class RdioScannerTalkgroup: NSObject, ObservableObject {
    let id : Int
    let alphaTag : String
    let desc : String
    let group : String
    let tag : String
    @Published var enabled : Bool = false {
        didSet {
            dirty = oldValue != enabled
        }
    }
    var dirty : Bool = false // Track if enabled changed

    init(talkgroup: RdioScannerSystemsQuery.Data.RdioScannerSystem.Talkgroup) {
        self.id = talkgroup.dec!
        self.alphaTag = talkgroup.alphaTag!
        self.desc = talkgroup.description!
        self.group = talkgroup.group!
        self.tag = talkgroup.tag!
    }
}
