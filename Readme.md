# wlkie-tlkie


A mobile (iOS) frontend for [my fork](https://gitlab.com/bettse/rdio-scanner/-/tree/apns) of [rdio-scanner](https://github.com/chuot/rdio-scanner).


![Screenshot](./screenshot.png "Screenshot (light mode)")

## Notes

 * You'll have to setup rdio-scanner (the apns branch of my work) and get your own push notification cert/key.

 * The playing call doesn't show in the call queue
 * Background playing is fickle, tap fast forward button to restart queue
 * Buttons on top control if new calls are automatically played
 * Configure your graphql endpoint and enable talkgroups in settings (gear icon)
